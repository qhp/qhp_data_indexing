import xml.etree.cElementTree as ElementTree
import zipfile, gzip
import os
import sys
import elasticsearch
import HTMLParser
import datetime
from multiprocessing import Process, Pool
from seconddatefinder import finddates2

import convert

global input_dir
input_dir = './vk-final/'

global es
#es = elasticsearch.Elasticsearch("localhost") #default to localhost; was zookst18:8009
#print 'indexing to localhost' #uncomment next line for real
es = elasticsearch.Elasticsearch("zookst18.science.uva.nl:8009",
    connection_class=elasticsearch.RequestsHttpConnection,
    http_auth="qhp:qhp")

INDEX_NAME = 'loedejong'

def index_name(doc):
    #year = int(doc["paper_dc_date"][:4])
    #if year < 1933 or year > 1949: return None
    #if year < 1939: return INDEX_NAME + "_ww2_before"
    #elif year > 1945: return INDEX_NAME + "_ww2_after"
    #else: 
    return INDEX_NAME# + "_ww2"

def index_document(doc_obj, _id):
    index = index_name(doc_obj)
    if index:
        es.index(index,"doc",doc_obj, id=_id)

def index_zipfile(zipfile_obj, _id):
    es.index(index=INDEX_NAME, doc_type="zipfile",  id=_id, body=zipfile_obj)

def write_progress(logfile):
    date_str = str(datetime.datetime.now())

    f = open(logfile, 'w')
    f.write(date_str)
    f.close()

#taken from http://stackoverflow.com/questions/7684333/converting-xml-to-dictionary-using-elementtree
def etree_to_dict(t):
    d = {t.tag : map(etree_to_dict, t.iterchildren())}
    d.update(('@' + k, v) for k, v in t.attrib.iteritems())
    d['text'] = t.text
    return d

def process_file(zipfilename):
    print "Processing:",zipfilename

    filename = zipfilename.split("/")[-1]
    logfile = './progress/' + filename.replace('.gz', '.log')
    print "Checking progress file %s" % logfile
    if os.path.exists(logfile):
        print "[%s] File already imported." % logfile
        return 1

    article_count = 0
    try:
        with gzip.GzipFile(zipfilename) as file:
            txt = file.read('utf-8')
            txt = txt.replace('&','&amp;') # quick and dirty solution to encoding entities
            #etree[0] = docinfo, etree[1] = meta etc
            #etree[0].tag = docinfo, etree[0].attrib is dict with attributes
            etree = ElementTree.fromstring(txt)

            #find children of indicated element with the specified tag
            #note sensitivity: might need a parent's xmlns specified as well
            #docs = etree.findall('{http://www.loedejongdigitaal.nl/docinfo}docinfo')
            
            
            #namespace to take care of xmlns
            namespaces = {'chapter': '{http://www.loedejongdigitaal.nl}'}
            #for every chapter
            for chapter in etree[2].findall('{http://www.loedejongdigitaal.nl}chapter'):
                #for every section
                for section in chapter.findall('{http://www.loedejongdigitaal.nl}section'):
                    
                    doc_obj = {}
                    contents = ''
                    for elem in section:
                        contents += ElementTree.tostring(elem)
                    #print contents
                    #attrs = list(section.attrib)
                    #
                    doc_obj['vk_id'] = section.attrib['{http://www.loedejongdigitaal.nl}id']
                    doc_obj['title'] = section.attrib['{http://www.loedejongdigitaal.nl}title']
                    pdfpage = section.find('{http://www.loedejongdigitaal.nl}p')
                    if pdfpage is not None:
                        doc_obj['pdfpage'] = pdfpage.attrib['{http://www.loedejongdigitaal.nl}pdf-page-ref']
                    else:
                        doc_obj['pdfpage'] = -1



                    #doesnt recognise type properly for some reason, so why not by hand.. convert.ConvertXmlToDict(etree)
                    doc_obj['vkbook_id'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}identifier')
                    doc_obj['format'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}format')
                    doc_obj['type'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}type')
                    doc_obj['coverage'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}identifier')
                    doc_obj['coverage'] = "World War II"
                    doc_obj['creator'] = "De Jong, Loe"
                    doc_obj['contributor'] = "Boer, V. de"
                    doc_obj['contributor'] = "Buitinck, L."
                    doc_obj['contributor'] = "Doornik, J. van"
                    doc_obj['contributor'] = "Grootveld, M."
                    doc_obj['contributor'] = "Marx, M."
                    doc_obj['contributor'] = "Ribbens, K."
                    doc_obj['contributor'] = "Veken, T."
                    doc_obj['creator'] = "De Jong, Loe"
                    doc_obj['publisher'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}publisher')
                    doc_obj['rights'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}rights')
                    doc_obj['book_date'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}date')
                    doc_obj['book_title'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}title')
                    doc_obj['book_description'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}description')
                    doc_obj['source'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}source')
                    doc_obj['relation'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}relation')
                    doc_obj['language'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}language')


                    # Unescape HTML entities
                    contents = contents.replace("&amp;amp;", '&')
                    contents = contents.replace("&amp;quot;", '"')
                    contents = contents.replace("&amp;gt;", '>')
                    contents = contents.replace("&amp;lt;", '<')
                    contents = contents.replace("&amp;apos;", "'")
                    
                    doc_obj['content'] = contents

                    dates = finddates2(contents)
                    doc_obj['firstdate'] = dates[0] + "-01-01"
                    doc_obj['seconddate'] = dates[1] + "-01-01"


                    # upload_document
                    doc_obj['zipfilename'] = zipfilename
                    index_document(doc_obj, doc_obj['vk_id'])

                    article_count += 1
            
            #for event, elem in ElementTree.iterparse(file):

            """
            for elem in etree:
                if elem.tag == 'doc':
                    doc_el = elem

                    doc_obj = {}
                    # paper metadata
                    doc_obj['paper_dc_title'] = doc_el.find("field[@name='dc_title']").text
                    doc_obj['paper_dc_identifier'] = doc_el.find("field[@name='dc_identifier']").text
                    doc_obj['paper_dcterms_alternative'] = doc_el.find("field[@name='dcterms_alternative']").text
                    doc_obj['paper_dcterms_isVersionOf'] = doc_el.find("field[@name='dcterms_isVersionOf']").text
                    doc_obj['paper_dc_date'] = doc_el.find("field[@name='dc_date']").text
                    doc_obj['paper_dcterms_temporal'] = doc_el.find("field[@name='dcterms_temporal']").text
                    doc_obj['paper_dcx_recordRights'] = doc_el.find("field[@name='dcx_recordRights']").text 
                    doc_obj['paper_dc_publisher'] = doc_el.find("field[@name='dc_publisher']").text
                    doc_obj['paper_dcterms_spatial'] = doc_el.find("field[@name='dcterms_spatial']").text
                    doc_obj['paper_dc_source'] = doc_el.find("field[@name='dc_source']").text
                    doc_obj['paper_dcx_volume'] = doc_el.find("field[@name='dcx_volume']").text
                    doc_obj['paper_dcx_issuenumber'] = doc_el.find("field[@name='dcx_issuenumber']").text
                    doc_obj['paper_dcx_recordIdentifier'] = doc_el.find("field[@name='dcx_recordIdentifier']").text
                    doc_obj['paper_dc_identifier_resolver'] = doc_el.find("field[@name='dc_identifier_resolver']").text
                    doc_obj['paper_dc_language'] = doc_el.find("field[@name='dc_language']").text
                    doc_obj['paper_dcterms_isPartOf'] = doc_el.find("field[@name='dcterms_isPartOf']").text
                    doc_obj['paper_ddd_yearsDigitized'] = doc_el.find("field[@name='ddd_yearsDigitized']").text
                    doc_obj['paper_dcterms_spatial_creation'] = doc_el.find("field[@name='dcterms_spatial_creation']").text
                    doc_obj['paper_dcterms_issued'] = doc_el.find("field[@name='dcterms_issued']").text

                    # article metadata
                    doc_obj['article_dc_subject'] = doc_el.find("field[@name='dc_subject']").text
                    doc_obj['article_dc_title'] = doc_el.findall("field[@name='dc_title']")[1].text
                    doc_obj['article_dcterms_accessRights'] = doc_el.find("field[@name='dcterms_accessRights']").text
                    doc_obj['article_dcx_recordIdentifier'] = doc_el.findall("field[@name='dcx_recordIdentifier']")[1].text
                    doc_obj['article_dc_identifier_resolver'] = doc_el.findall("field[@name='dc_identifier_resolver']")[1].text
                    doc_obj['paper_dc_language'] = doc_el.find("field[@name='dc_language']").text
                    doc_obj['paper_dcterms_isPartOf'] = doc_el.find("field[@name='dcterms_isPartOf']").text
                    doc_obj['paper_ddd_yearsDigitized'] = doc_el.find("field[@name='ddd_yearsDigitized']").text
                    doc_obj['paper_dcterms_spatial_creation'] = doc_el.find("field[@name='dcterms_spatial_creation']").text
                    doc_obj['paper_dcterms_issued'] = doc_el.find("field[@name='dcterms_issued']").text

                    # article metadata
                    doc_obj['article_dc_subject'] = doc_el.find("field[@name='dc_subject']").text
                    doc_obj['article_dc_title'] = doc_el.findall("field[@name='dc_title']")[1].text
                    doc_obj['article_dcterms_accessRights'] = doc_el.find("field[@name='dcterms_accessRights']").text
                    doc_obj['article_dcx_recordIdentifier'] = doc_el.findall("field[@name='dcx_recordIdentifier']")[1].text
                    doc_obj['article_dc_identifier_resolver'] = doc_el.findall("field[@name='dc_identifier_resolver']")[1].text

                    # text content
                    content_el = doc_el.find("field[@name='content']")
                    text_el = content_el.find("text") #sometimes no text_el found (invalid ocr xml)
                    if text_el is None:
                        doc_obj['text_content'] = ''
                    else:
                        try:
                            text_content = "\n\n".join(el.text for el in text_el.findall("p") if el.text)
                        except:
                            print ElementTree.tostring(text_el)
                            raise

                        # Unescape HTML entities
                        text_content = text_content.replace("&amp;amp;", '&')
                        text_content = text_content.replace("&amp;quot;", '"')
                        text_content = text_content.replace("&amp;gt;", '>')
                        text_content = text_content.replace("&amp;lt;", '<')
                        text_content = text_content.replace("&amp;apos;", "'")

                        #if doc_obj['article_dcx_recordIdentifier'] == 'ddd:000013812:mpeg21:a0010':
                        #       print text_content

                        doc_obj['text_content'] = text_content
                    # upload_document
                    doc_obj['zipfilename'] = zipfilename
                    doc_obj['identifier'] = doc_obj['article_dcx_recordIdentifier']
                    index_document(doc_obj, doc_obj['identifier'])

                    article_count += 1
                    """

        #zipfile_obj = {}
        #zipfile_obj['zipfilename'] = zipfilename
        #ipfile_obj['article_count'] = article_count

        #logname_arr = zipfilename.split("/")
        #index_zipfile(zipfile_obj, logname_arr[4])
        write_progress(logfile)
        return (article_count)  
    except Exception as error:
        print type(error), "in", zipfilename + ":", error
        import traceback
        traceback.print_exc()
        return (0)




if __name__  == '__main__':
    pool = Pool(processes=16)
    results = []

    for filename in os.listdir(input_dir):
#        print process_file(input_dir + filename)
#        assert False
        # Do not forget to run async
        result = pool.apply_async(process_file, [input_dir + filename])
        results.append(result)
    pool.close()

    
    pool.join()
