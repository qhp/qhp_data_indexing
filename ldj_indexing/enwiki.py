import xml.etree.cElementTree as ElementTree
import zipfile, gzip
import os
import sys
import elasticsearch
import HTMLParser
import datetime
from multiprocessing import Process, Pool
from datefinder import finddates
from time import sleep

import convert

global input_dir
input_dir = './vk-final/'

global es
es = elasticsearch.Elasticsearch("zookst18.science.uva.nl:8009",
    connection_class=elasticsearch.RequestsHttpConnection,
    http_auth="qhp:qhp") #default to localhost; was zookst18:8009

INDEX_NAME = 'enwiki'

def index_name(doc):
    #year = int(doc["paper_dc_date"][:4])
    #if year < 1933 or year > 1949: return None
    #if year < 1939: return INDEX_NAME + "_ww2_before"
    #elif year > 1945: return INDEX_NAME + "_ww2_after"
    #else: 
    return INDEX_NAME# + "_ww2"

def index_document(doc_obj, _id):
    index = index_name(doc_obj)
    if index:
        es.index(index,"doc",doc_obj, id=_id)

def index_zipfile(zipfile_obj, _id):
    es.index(index=INDEX_NAME, doc_type="zipfile",  id=_id, body=zipfile_obj)

def write_progress(logfile):
    date_str = str(datetime.datetime.now())

    f = open(logfile, 'w')
    f.write(date_str)
    f.close()

#taken from http://stackoverflow.com/questions/7684333/converting-xml-to-dictionary-using-elementtree
def etree_to_dict(t):
    d = {t.tag : map(etree_to_dict, t.iterchildren())}
    d.update(('@' + k, v) for k, v in t.attrib.iteritems())
    d['text'] = t.text
    return d

def process_file(zipfilename):

    returnval = 0
    
    #for testing: retrieve all those /with/ date1 fields
    query = {"query" : { "filtered" : { "filter" : { "exists" : { "field" : "date1" } } } } }
    test=es.search(index="enwiki",body=query)
    size=test['hits']['total']
    print 'done: ' + str(size)
    query = {"query" : { "filtered" : { "filter" : { "not" : { "exists" : { "field" : "date1" } } } } } }
  
    #get total number of hits
    test=es.search(index="enwiki",body=query)
    size=test['hits']['total'] 
    while(size != 0):
        print 'new attempt; ' + str(size) + ' left'
    
        #now also search with the size we just found
        query = {"query" : { "filtered" : { "filter" : { "not" : { "exists" : { "field" : "date1" } } } } }, "size" : 100 }

        try:
            response=es.search(index="enwiki",body=query)
        #scan approach didnt work so im doing it somewhat uglier. remnants of code:
#        scanResp = es.search(index="nlwiki", body=query, search_type="scan", scroll="1m")  
#        scrollId = scanResp['_scroll_id']
#        print 'id1 :' + str(scrollId)

#        response = es.scroll(scroll_id = scrollId, scroll = "10m")
#        scrollId = response['_scroll_id']
#        print 'id2 :' + str(scrollId)
        #response is dict with attr hits; for dicts do for k, v in dict.iteritems():
#        while(response):

            #hits hits is a list
            #Consider all articles returned in this scroll
            for article in response['hits']['hits']:
#                print article['_source']
                #store id
                a_id = article['_id']
                a_text = article['_source']['text']
                a_dates = finddates(a_text)
                a_query = {"doc" : {"date1" : a_dates[0], "date2" : a_dates[1]}}
                es.update(index="enwiki", doc_type="page", id=a_id, body=a_query)
            #for checking
           ##     res = es.get(index="enwiki", id=a_id)

        except Exception as error:
            print type(error), ":", error
            import traceback
            traceback.print_exc()
            return (0)
        
        query = {"query" : { "filtered" : { "filter" : { "not" : { "exists" : { "field" : "date1" } } } } } }
        test = es.search(index="enwiki",body=query)
        size = test['hits']['total']
        #print size 
    return (returnval)  



if __name__  == '__main__':
    #pool = Pool(processes=16)
    #results = []

    #for filename in os.listdir(input_dir):
#        print process_file(input_dir + filename)
#        assert False
        # Do not forget to run async
    #    result = pool.apply_async(process_file, [input_dir + filename])
    #    results.append(result)
    #pool.close()

    
    #pool.join()

    #lets not pool for now
    returnval = process_file('unused')
