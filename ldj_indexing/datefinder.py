import re
from collections import Counter

#possible extension: track words in front of year and store the one used most
#in front of that year (break ties by using the one first used in the article).
#especially for wiki articles finding dates should be possible, considering
#a standard formatting http://en.wikipedia.org/wiki/Wikipedia:Date_formattings

def finddates(text):
    #find occurrences of 'xx, 19xx and 20xx that aren't preceded or followed
    #by a number
    dates = re.findall("((?<!\d)(19|20|')\d{2}(?!\d))", text)

    #get rid of the (year|yeargroup) representation + combine different
    #representations of the same year
    i = 0
    while(i < len(dates)):
        if(dates[i][1] == '20'):
            dates[i] = dates[i][0]
        else:
            dates[i] = '19' + dates[i][0][-2:]

        i += 1

    #count how often each year occurs, store the 2 most common in order
    counts = Counter(dates).most_common(2)
    print counts

    #find first and second year values
    #default years
    firstyear = "N/A"
    secondyear = "N/A"

    if(len(counts) > 0):
        firstyear = counts[0][0]
    #if a second year occurs more than twice and more than a third as much as
    #the first year
    if(len(counts) > 1 and counts[1][1] > 2 and counts[1][1] * 3 > counts[0][1]):
        secondyear = counts[1][0]

    return (firstyear, secondyear)

#example of use
print finddates("this is'45 '45a t21901ext 1945 written in 2014 2014 2014 '45 '45 '45 '45 '45 ");
#print finddates("one year '45")
#print finddates("no years")
