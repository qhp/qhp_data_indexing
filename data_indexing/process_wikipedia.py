
# coding: utf-8

# In[1]:

import elasticsearch
from elasticsearch.helpers import scan
es = elasticsearch.Elasticsearch(host="qhp", port=80, http_auth=("qhp", "qhp"),
                                 connection_class=elasticsearch.connection.Urllib3HttpConnection)


# In[2]:

from datetime import datetime
timestamp = str(datetime.now())[:-7].replace(" ", "T")
print timestamp


# In[3]:

import sys
sys.path.insert(0, "/home/ggarbac1/xtas.heideltime/")

from xtas.tasks import es_document, heideltime, fetch
from xtas.tasks.es import store_single
from lxml import html
import re
from joblib import Memory
import datetime

#memory = Memory("/tmp/ocr_heideltime.cache", verbose=0)
#heideltime = memory.cache(heideltime)
#fetch = memory.cache(fetch)

abbreviations = {
    "jan.": "januari",
    "febr.": "februari",
    "apr.": "april",
    "aug.": "aug",
    "sept.": "september",
    "okt.": "oktober",
    "nov.": "november",
    "dec.": "december",
}

def accept_date(date):
    if "_REF" in date: return False
    if date.startswith("P"): return False
    if date.startswith("XXXX-WXX"): return False
    if date.startswith("XXXX-XX"): return False
    if len(date) < 4: return False
    return True

def qhp_heideltime(index, doc_type, doc_id, field, **kwargs):
    title = fetch(es_document(index, doc_type, doc_id, "title"))
    doc = fetch(es_document(index, doc_type, doc_id, field))
    if doc != "": doc = html.fromstring(doc).text_content()
    return ocr_heideltime(title + "\n\n" + doc, **kwargs)

def fix_heideltime_text(text):
    fixed_text = re.sub(r"\s*'\s*(\d\d)([^\d])", r" 19\1\2", text)
    fixed_text = re.sub(r" van (\d{4})", r" \1", fixed_text)
    fixed_text = re.sub(r"(\d{4})\s*[-\\]\s*(\d{4})", r"\1 \2", fixed_text)
    fixed_text = fixed_text.replace("(", " ").replace(")", " ").replace("  ", " ")
    fixed_text = re.sub(r":(\d)", r": \1", fixed_text) # bar:2005
    fixed_text = re.sub(r"[^\.]\n", ".\n", fixed_text)
    for abbr in abbreviations.iteritems():
        fixed_text = fixed_text.replace(*abbr)
    return fixed_text

def fix_ocr_text(text):
    fixed_text = re.sub(r"I\s?I", r"II", text)
    fixed_text = re.sub(r"I", r"1", fixed_text)
    fixed_text = re.sub(r"°", r"0", fixed_text)
    fixed_text = re.sub(r"S", r"5", fixed_text)
    return fixed_text

def ocr_heideltime(text, fix_heideltime=True, fix_ocr=True, combine=False, debug=False):
    if fix_ocr: text = fix_ocr_text(text)
    if fix_heideltime: text = fix_heideltime_text(text)
    if debug: print text
    dates = heideltime(text, language="dutch")
    if combine: dates += heideltime(text, language="english")
    dates = [date[:4] if date[-2:] in "SP WI FA SU".split() else date for date in dates if accept_date(date)]
    return set(dates)

def isValidDate(date):
    validDate = 1
    try:
        datetime.datetime.strptime(date, '%Y-%m-%d')
        if(date < '1933-01-01' or date >= '1950-01-01'):
            validDate = 0 
    except ValueError:
        validDate = 0
    return validDate

def isValidMonth(date):
    validMonth = 1
    try:
        datetime.datetime.strptime(date, '%Y-%m')
        if(date < '1933-01' or date >= '1950-01'):
            validMonth = 0
    except ValueError:
        validMonth = 0
    return validMonth

def isValidYear(date):
    validYear = 1
    try:
        datetime.datetime.strptime(date, '%Y')
        if(date < '1933' or date >= '1950'):
            validYear = 0
    except ValueError:
        validYear = 0
    return validYear

def convertToDate(date):
    year, month, day = map(int, date.split("-"))
    properDate = datetime.date(year, month, day)
    return properDate
"""
# In[76]:

heideltime("in de eerste maanden van 1945 ca. z", language="dutch")


# In[117]:

ocr_heideltime("1940-1945", fix_ocr=False)


# In[77]:

fix_ocr_text("in de eerste maanden van '45 ")


# In[78]:

qhp_heideltime('loedejong', 'doc', 'nl.vk.d.11b-1.7.4', 'content')


# In[155]:

heideltime("28 september 1945", language="dutch")


# In[ ]:
"""
from joblib import Parallel, delayed

#Date extraction

# Parallel version
def processResults(results):
    jobs = [delayed(processResult)(results[i]) for i in range(len(results))]
    return Parallel(n_jobs=5, verbose=10, backend="threading")(jobs)

"""
#process documents from the index
def processResult(result):
    try:
        #print "current doc", result["_id"]
        dates = qhp_heideltime("wikipedia", "page", result["_id"], "plain_text", fix_ocr=False, combine=False)
        #print "dates", dates
        dates = list(dates)
        es.update("wikipedia", "page", result["_id"], body={"doc":{"dates_identified":dates}})
    except:
        print "ISSUE"

# Non-parallel version
def processResults(results):
    for i in range(len(results)):
        try:
            print "current doc", results[i]["_id"]
            dates = qhp_heideltime("wikipedia", "page", results[i]["_id"], "plain_text", fix_ocr=False, combine=False)
            print "dates", dates
            dates = list(dates)
            es.update("wikipedia", "page", results[i]["_id"], body={"doc":{"dates_identified":dates}})
        except:
            print "ISSUE"
"""

#Split dates
def processResult(result):
    try:
        #dates = fetch(es_document("wikipedia", "page", result["_id"], "dates_identified"))
        dates = es.get(index="wikipedia", doc_type="page", id=result["_id"])["_source"]["dates_identified"]
        #print "dates", dates

        yearMentions = set()
        monthMentions = set()
        dayMentions = set()
        uncertainDates = set()

        for date in dates:
            if(isValidDate(date)):
                completeDate = convertToDate(date)
                dayMentions.add(completeDate)
                # we need to append to months and years as well
                year, month, day = map(int, date.split("-"))
                monthMentions.add(datetime.date(year, month, 1))
                yearMentions.add(datetime.date(year, 1, 1))
            elif(isValidMonth(date)):
                completeDate = convertToDate(date + "-01")
                monthMentions.add(completeDate)
                # we need to append to years as well
                year, month = map(int, date.split("-"))
                yearMentions.add(datetime.date(year, 1, 1))
            elif(isValidYear(date)):
                completeDate = convertToDate(date + "-01-01")
                yearMentions.add(completeDate)
            elif(((date.startswith("XXXX-")
                                    or (("-XX") in date and int(date[:4]) >= 1933 and int(date[:4]) < 1950 )
                                    or ((date.endswith("-WI") or date.endswith("-SU") or date.endswith("-FA") or date.endswith("-SP")) and int(date[:4]) >= 1933 and int(date[:4]) < 1950 ))
                  or (date.startswith("PAST_REF"))
                  or (date.startswith("PRESENT_REF"))
                  or (date.startswith("FUTURE_REF")))):
                uncertainDates.add(date)
        
        #print "Day mentions: ", sorted(dayMentions)
        #print "Month mentions: ", sorted(monthMentions)
        #print "Year mentions: ", sorted(yearMentions)
        #print "Uncertain dates: ", sorted(uncertainDates)
        es.update('wikipedia', 'page', result["_id"], body={"doc":{"years":sorted(list(yearMentions))}})
        es.update('wikipedia', 'page', result["_id"], body={"doc":{"months":sorted(list(monthMentions))}})
        es.update('wikipedia', 'page', result["_id"], body={"doc":{"days":sorted(list(dayMentions))}})
        es.update('wikipedia', 'page', result["_id"], body={"doc":{"uncertain_dates":sorted(list(uncertainDates))}})
    except Exception as error:
        print type(error), error
        import traceback
        traceback.print_exc()
        return (0)

totalDocs = es.search("wikipedia", body={"query":{"match_all":{}}})["hits"]["total"]
#totalDocs = es.search('wikipedia', body={"query":{"bool":{"must":[{"constant_score":{"filter":{"missing":{"field":"dates_identified"}}}}],"must_not":[],"should":[]}},"from":0,"size":0,"sort":[],"facets":{}})['hits']['total']
print "total docs", totalDocs


index = "wikipedia"
query = {"query": {"match_all": {}}}

i=1
for doc in scan(es, query = query, index = index):
    print "currentIteration = ", i, " doc id = " + doc["_id"]
    processResult(doc)
    i += 1

"""
currentStartIndex = 0
while (currentStartIndex <= totalDocs):
    print "Current index is", currentStartIndex
    results = es.search('wikipedia', body={"query":{"match_all":{}}, "from": currentStartIndex, "size": 5000})['hits']['hits']
    #results = es.search('wikipedia', body={"query":{"bool":{"must":[{"constant_score":{"filter":{"missing":{"field":"dates_identified"}}}}],"must_not":[],"should":[]}},"from":currentStartIndex,"size":5000,"sort":[],"facets":{}})['hits']['hits']
    print "total results", len(results)
    #extract dates
    processResults(results)
    currentStartIndex += 5000
    print "Newly updated current index is", currentStartIndex

print "Done"
"""

"""
# In[11]:

from collections import defaultdict
files_ldj = get_ipython().getoutput(u'ls data/LoeDeJong/*.ann')
results, date_count, unique_count, ocr_error_count = [], 0, 0, 0
annotations = defaultdict(dict)
for filename in files_ldj:
    with open(filename) as ann: lines = ann.readlines()
    grouped_lines = defaultdict(lambda: defaultdict(list))
    for line in lines:
        if line[0] == "T": key = line.split('\t')[0]
        else: key = line.split('\t')[1].split()[-1]
        grouped_lines[key][line[0]] += line,
    annotated = set()
    for tid, typed_lines in grouped_lines.iteritems():
        assert len(typed_lines["T"]) == 1 and len(typed_lines["#"]) == 1, "Error for %s in %s" % (tid, filename)
        label = typed_lines["T"][0].strip().split("\t")[-1]
        date = typed_lines["#"][0].strip().split("\t")[-1].strip()
        attributes = map(lambda l: l.strip().split("\t")[1].split()[0], typed_lines["A"])
        if len(tid) > 3 and tid.startswith("T99"): continue
        assert date
        if "OtherGranularity" in attributes: continue
        #if attributes: continue
        if not "agree." in filename: 
            date_count += 1
            if "OCRerror" in attributes: ocr_error_count += 1
        if(date[-2:] in "SP WI FA SU".split()): date = date[:4]
        annotations[filename][tid] = date
        annotated.add(date)
        if "-" in date and not "X" in date:
            annotated.add("-".join(date.split("-")[:2]))
            annotated.add(date.split("-")[0])
        #print date, "<-", label
    if not annotated: continue
    if not "agree." in filename: unique_count += len(annotated)
    doc_id = filename[:-4].split("/")[-1].replace("agree.", "")
    #heideltime = set(es.get(index="loedejong", id=doc_id)["_source"]["xtas_results"]['heideltime']['data'])
    #extracted = qhp_heideltime('loedejong', 'doc', doc_id, 'content', fix_heideltime=True, fix_ocr=True, combine=True)
    extracted = qhp_heideltime('loedejong', 'doc', doc_id, 'content', fix_heideltime=False, fix_ocr=False, combine=False)
#    for date in annotated-heideltime: print "MISSED:", date
#    for date in heideltime-annotated: print "FP:", date
    precision = float(len(annotated&extracted))/len(extracted)
    recall = float(len(annotated&extracted))/len(annotated)
    f1 = 2*precision*recall/(precision+recall) if (precision+recall) else 0.0
    print "%.4f %.4f %.4f" % (precision, recall, f1),
    print filename[5:]
    if not "agree." in filename: results += (precision, recall, f1),
    print 20*"-"
    with open(filename, "w") as ann:
        for line in lines:
            if line[1:3] == "99" and line[3] != "\t": continue
            if line.split("\t")[0] in annotations[filename]:
                missed = annotations[filename][line.split("\t")[0]] not in extracted
                if missed: line = line.replace("Date ", "DateMissed ")
                else: line = line.replace("DateMissed ", "Date ")
            ann.write(line)
        
        result = "P%.4f R%.4f F%.4f" % (precision, recall, f1)
        ann.write("T9900\tResult 0 0\t\n")
        ann.write("#9900\tAnnotatorNotes T9900\t%s\n" % result)

        for index, date in enumerate(extracted-annotated):
            ann.write("T99%02d\tDateFalsePositive 0 0\t\n" % (index+1))
            ann.write("#99%02d\tAnnotatorNotes T99%02d\t%s\n" % (index+1, index+1, date))

print "%.4f %.4f %.4f" % tuple([m/len(results) for m in map(sum, zip(*results))]),
print len(results), "documents"
print "OCR Errors in %d of %d date references (%.4f). %d of which were unique (%.4f)." % (ocr_error_count, date_count, float(ocr_error_count)/date_count, unique_count, float(unique_count)/date_count)


# In[231]:

for filename in annotations:
    if not "agree." in filename: continue
    if not filename.replace("agree.", "") in annotations: continue
    a = set(annotations[filename].itervalues())
    b = set(annotations[filename.replace("agree.", "")].itervalues())
    print "%.2f" % (float(len(a&b))/len(a|b)), filename


# In[39]:

from collections import defaultdict
files_wp = get_ipython().getoutput(u'ls data/Wikipedia/*.ann')
results, date_count, unique_count = [], 0, 0
annotations = defaultdict(dict)
for filename in files_wp:
    with open(filename) as ann: lines = ann.readlines()
    grouped_lines = defaultdict(lambda: defaultdict(list))
    for line in lines:
        if line[0] == "T": key = line.split('\t')[0]
        else: key = line.split('\t')[1].split()[-1]
        grouped_lines[key][line[0]] += line,
    annotated = set()
    for tid, typed_lines in grouped_lines.iteritems():
        assert len(typed_lines["T"]) == 1 and len(typed_lines["#"]) == 1, "Error for %s in %s" % (tid, filename)
        label = typed_lines["T"][0].strip().split("\t")[-1]
        date = typed_lines["#"][0].strip().split("\t")[-1].strip()
        attributes = map(lambda l: l.strip().split("\t")[1].split()[0], typed_lines["A"])
        if tid.startswith("T99"): continue
        assert date
        if len(attributes): continue
        if(date[-2:] in "SP WI FA SU".split()): date = date[:4]
        if not "agree." in filename: date_count += 1
        annotations[filename][tid] = date
        annotated.add(date)
        if "-" in date and not "X" in date:
            annotated.add("-".join(date.split("-")[:2]))
            annotated.add(date.split("-")[0])
        #print date, "<-", label
    if not annotated: continue
    if not "agree." in filename: unique_count += len(annotated)
    doc_id = filename[:-4].split("/")[-1].replace("agree.", "")
    #extracted = set(es.get(index="nlwikidated5", id=doc_id)["_source"]["xtas_results"]['heideltime']['data'])
    extracted = qhp_heideltime("nlwikidated5", "page", doc_id, "plain_text", fix_ocr=False, combine=False)
    #for date in annotated-extracted: print "MISSED:", date
    #for date in extracted-annotated: print "FP:", date
    precision = float(len(annotated&extracted))/len(extracted)
    recall = float(len(annotated&extracted))/len(annotated)
    f1 = 2*precision*recall/(precision+recall)
    print "%.4f %.4f %.4f" % (precision, recall, f1),
    print filename[5:]
    print 20*"-"
    if not "agree." in filename: results += (precision, recall, f1),
    with open(filename, "w") as ann:
        for line in lines:
            if line[1:3] == "99": continue
            if line.split("\t")[0] in annotations[filename]:
                missed = annotations[filename][line.split("\t")[0]] not in extracted
                if missed: line = line.replace("Date ", "DateMissed ")
                else: line = line.replace("DateMissed ", "Date ")
            ann.write(line)
        
        result = "P%.4f R%.4f F%.4f" % (precision, recall, f1)
        ann.write("T9900\tResult 0 0\t\n")
        ann.write("#9900\tAnnotatorNotes T9900\t%s\n" % result)

        for index, date in enumerate(extracted-annotated):
            ann.write("T99%02d\tDateFalsePositive 0 0\t\n" % (index+1))
            ann.write("#99%02d\tAnnotatorNotes T99%02d\t%s\n" % (index+1, index+1, date))

print "%.4f %.4f %.4f" % tuple([m/len(results) for m in map(sum, zip(*results))]),
print len(results), "documents"
print "%d date references. %d of which were unique." % (date_count, unique_count)


# In[233]:

for filename in annotations:
    if not "agree." in filename: continue
    if not filename.replace("agree.", "") in annotations: continue
    a = set(annotations[filename].itervalues())
    b = set(annotations[filename.replace("agree.", "")].itervalues())
    print "%.2f" % (float(len(a&b))/len(a|b)), filename


# In[35]:

counts = []
files_ldj = get_ipython().getoutput(u'ls data/LoeDeJong/*.ann')
for filename in files_ldj:
    if "agree." in filename: continue
    results = es.search(index="loedejong", size=0, timeout=0, body={"facets": {"terms": {
      "terms": {
        "field": "_all",
        "size": 10**8
      }
    }}}, q="_id:" + filename.split("/")[-1][:-4])
    counts += sum(t["count"] for t in results["facets"]["terms"]["terms"]),
sum(counts)


# In[37]:

counts = []
files_wp = get_ipython().getoutput(u'ls data/Wikipedia/*.ann')
for filename in files_wp:
    if "agree." in filename: continue
    results = es.search(index="nlwikidated5", size=0, timeout=0, body={"facets": {"terms": {
      "terms": {
        "field": "_all",
        "size": 10**8
      }
    }}}, q="_id:" + filename.split("/")[-1][:-4])
    counts += sum(t["count"] for t in results["facets"]["terms"]["terms"]),
sum(counts)


# In[33]:

es = elasticsearch.Elasticsearch(host="zookst18", port=8009)


# In[177]:

qhp_heideltime("nlwikidated5", "page", "2796484", "plain_text", debug=True)


# In[150]:

heideltime("De A555 werd tussen 1929 en 1932 gebouwd en werd in augustus 1932 door Konrad Adenauer als vierstrooks-, kruisingsvrije 12 meter brede autoweg geopend. Daarmee is de A555 de oudste autosnelweg van Duitsland. De AVUS in Berlijn is weliswaar ouder, maar dit was een testtraject met tolplicht. De snelweg werd echter alleen een Landstrasse en werd pas in 1958 omgenummerd tot autosnelweg. Vlak voor de opening werd door de politie een speciale verordening afgegeven, dat keren, stoppen en parkeren, alsmede het gebruik van de weg door niet-gemotoriseerd verkeer verbood.Tot de verbreding tot 3 rijstroken in beide richtingen was de A555 niet uitgevoerd met een fysieke rijbaanscheiding. Tussen de rijstroken was ook geen belijning aangebracht, alleen de verschillende rijrichtingen werden door een dikke middellijn van elkaar gescheiden. De weg was ingericht voor een maximumsnelheid van 120 kilometer per uur, terwijl slechts weinig voertuigen deze snelheid konden behalen begin jaren 30. 1n het eerste jaar kende de A555 een verkeersintensiteit 4000 voertuigen. Tegenwoordig wordt de weg door 100.000 voertuigen per dag bereden.", language="dutch")


# In[182]:

heideltime("Essai de determination du radiant apparent d un essaim d etoiles filantes a l aide de documents photographiques, Brussel, Palais des Academies, 1935\n Etude des objets celestes resultants, Gembloux, 1mprimerie Jules Duculot, 1949\n Un arstrographe a champs des 180deg, 1mprimerie lAvenir , 1951  ".replace("\n", " .\n"), language="dutch")
"""
