import xml.etree.cElementTree as ElementTree
import zipfile, gzip
import os
import sys
import elasticsearch
import HTMLParser
import datetime
from multiprocessing import Process, Pool

global input_dir
input_dir = '/home/ggarbac1/QHP_Project/qhpvis/data_indexing/vk-final/'

global es
es = elasticsearch.Elasticsearch("zookst18.science.uva.nl:8009",
    connection_class=elasticsearch.RequestsHttpConnection,
    http_auth="qhp:qhp")

INDEX_NAME = 'books'

def index_name(doc):
    return INDEX_NAME

def index_document(doc_obj, _id):
    index = index_name(doc_obj)
    if index:
        es.index(index,"doc",doc_obj, id=_id)

def index_zipfile(zipfile_obj, _id):
    es.index(index=INDEX_NAME, doc_type="zipfile",  id=_id, body=zipfile_obj)

def write_progress(logfile):
    date_str = str(datetime.datetime.now())

    f = open(logfile, 'w')
    f.write(date_str)
    f.close()

def etree_to_dict(t):
    d = {t.tag : map(etree_to_dict, t.iterchildren())}
    d.update(('@' + k, v) for k, v in t.attrib.iteritems())
    d['text'] = t.text
    return d

def process_file(zipfilename):
    print "Processing:",zipfilename

    filename = zipfilename.split("/")[-1]
    logfile = 'progress/' + filename.replace('.gz', '.log')
    print "Checking progress file %s" % logfile
    if os.path.exists(logfile):
        print "[%s] File already imported." % logfile
        return 1

    article_count = 0
    try:
        with gzip.GzipFile(zipfilename) as file:
            print "File is", file
            txt = file.read('utf-8')
            txt = txt.replace('&','&amp;') # quick and dirty solution to encoding entities
            etree = ElementTree.fromstring(txt)
            #namespace to take care of xmlns
            namespaces = {'chapter': '{http://www.loedejongdigitaal.nl}'}
            #for every chapter
            for chapter in etree[2].findall('{http://www.loedejongdigitaal.nl}chapter'):
                #for every section
                for section in chapter.findall('{http://www.loedejongdigitaal.nl}section'):
                    doc_obj = {}
                    contents = ''
                    for elem in section:
                        contents += ElementTree.tostring(elem)
                    doc_obj['vk_id'] = section.attrib['{http://www.loedejongdigitaal.nl}id']
                    doc_obj['title'] = section.attrib['{http://www.loedejongdigitaal.nl}title']
                    pdfpage = section.find('{http://www.loedejongdigitaal.nl}p')
                    if pdfpage is not None:
                        doc_obj['pdfpage'] = pdfpage.attrib['{http://www.loedejongdigitaal.nl}pdf-page-ref']
                    else:
                        doc_obj['pdfpage'] = -1

                    #doesnt recognise type properly for some reason, so why not by hand.. convert.ConvertXmlToDict(etree)
                    doc_obj['vkbook_id'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}identifier')
                    doc_obj['format'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}format')
                    doc_obj['type'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}type')
                    doc_obj['coverage'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}identifier')
                    doc_obj['coverage'] = "World War II"
                    doc_obj['creator'] = "De Jong, Loe"
                    doc_obj['contributor'] = "Boer, V. de"
                    doc_obj['contributor'] = "Buitinck, L."
                    doc_obj['contributor'] = "Doornik, J. van"
                    doc_obj['contributor'] = "Grootveld, M."
                    doc_obj['contributor'] = "Marx, M."
                    doc_obj['contributor'] = "Ribbens, K."
                    doc_obj['contributor'] = "Veken, T."
                    doc_obj['creator'] = "De Jong, Loe"
                    doc_obj['publisher'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}publisher')
                    doc_obj['rights'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}rights')
                    doc_obj['book_date'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}date')
                    doc_obj['book_title'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}title')
                    doc_obj['book_description'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}description')
                    doc_obj['source'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}source')
                    doc_obj['relation'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}relation')
                    doc_obj['language'] = etree[1].findtext('{http://purl.org/dc/elements/1.1/}language')

                    # Unescape HTML entities
                    contents = contents.replace("&amp;amp;", '&')
                    contents = contents.replace("&amp;quot;", '"')
                    contents = contents.replace("&amp;gt;", '>')
                    contents = contents.replace("&amp;lt;", '<')
                    contents = contents.replace("&amp;apos;", "'")
                    doc_obj['content'] = contents

                    # upload_document
                    doc_obj['zipfilename'] = zipfilename
                    index_document(doc_obj, doc_obj['vk_id'])

                    article_count += 1
            
        write_progress(logfile)
        return (article_count)  
    except Exception as error:
        print type(error), "in", zipfilename + ":", error
        import traceback
        traceback.print_exc()
        return (0)

if __name__  == '__main__':
    pool = Pool(processes=16)
    results = []

    for filename in os.listdir(input_dir):
        # Do not forget to run async
        result = pool.apply_async(process_file, [input_dir + filename])
        results.append(result)
    pool.close()
    pool.join()
