from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
import ujson as json

es = Elasticsearch(host="zookst15.science.uva.nl", port=8004)

index = "tweets_v2"
query = {"query": {"match_all": {}}}

for doc in scan(es, query=query, index=index):
    print json.dumps(doc)
